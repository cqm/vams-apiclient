Instanciar el cliente:

```php
$api = new \VAMS\ApiClient\ApiClient([
    'endpoint' => 'https://vams.endpoint/api/',
    'api_key' => 'apikey',
    'sign_secret' => 'sign_secret',
    'crypt_secret' => 'crypt_secret', // Opcional
]);
```

Ejemplo listado de activos:

```php
try {
    $result = $api->get('assets/list_assets', [
        'category_slug' => 'deporte',
    ]);

    // Code
} catch (\VAMS\ApiClient\ApiException $ex) {
    // Error handling
}
```