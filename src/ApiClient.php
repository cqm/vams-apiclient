<?php

namespace VAMS\ApiClient;

use CURLFile;
use LogicException;
use VAMS\ApiClient\exceptions\NotificationDecodingException;
use VAMS\ApiClient\exceptions\NotificationException;
use VAMS\ApiClient\exceptions\SignatureMismatch;

class ApiClient
{

    /** @var array */
    private $config = [
        'endpoint' => '',
        'timeout' => 30,
        'api_key' => '',
        'sign_secret' => '',
        'crypt_secret' => '',
        'verify_ssl' => true,
        'ssl_certs_bundle' => __DIR__ . '/../assets/ca-bundle.pem',
    ];

    public function __construct(array $config = [])
    {
        $this->initConfig($config);
    }

    /**
     * Performs a GET request
     *
     * @param string $resource
     * @param array $params
     * @return Response
     * @throws ApiException
     */
    public function get($resource, array $params = [])
    {
        return $this->doRequest('get', $resource, $params);
    }

    /**
     * Performs a POST request
     *
     * @param string $resource
     * @param array $params
     * @return Response
     * @throws ApiException
     */
    public function post($resource, array $params = [])
    {
        return $this->doRequest('post', $resource, $params);
    }

    /**
     * Performs a request
     *
     * @param string $resource
     * @param array $params
     * @param string $http_method
     * @return Response
     * @throws ApiException
     */
    public function call($resource, array $params = [], $http_method = 'get')
    {
        return $this->doRequest($http_method, $resource, $params);
    }

    /**
     * Returns the notification data
     *
     * @return array
     * @throws NotificationDecodingException
     * @throws SignatureMismatch
     * @throws NotificationException
     * @throws NotificationException
     */
    public function getNotificationData($objects_as_arrays = false)
    {
        $data = filter_input(INPUT_POST, 'params');
        $signature = filter_input(INPUT_POST, 'signature');

        if (!$data) {
            throw new NotificationException('Missing data in request body');
        }
        if (!$signature) {
            throw new NotificationException('Missing signature in request body');
        }

        if (!$this->validateSignature($data, $signature)) {
            throw new SignatureMismatch('Signature mismatch');
        }

        $data = $this->decryptData($data);
        $data = json_decode($data, $objects_as_arrays);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotificationDecodingException('Error decoding notification');
        }

        return $data;
    }

    /**
     * Validates signature
     *
     * @param string $data
     * @param string $signature
     * @return bool
     */
    public function validateSignature($data, $signature)
    {
        $expected_signature = hash_hmac('sha256', $data, $this->config['sign_secret']);

        return hash_equals($expected_signature, $signature);
    }

    /**
     * Decrypts encrypted data
     *
     * @param string $data
     * @return string
     * @throws LogicException
     */
    public function decryptData($data)
    {
        if (empty($this->config['crypt_secret'])) {
            throw new LogicException('The crypt secret is not configured.');
        }

        $raw = base64_decode($data);
        $iv_length = openssl_cipher_iv_length('AES-256-CBC');

        // Split data
        $iv = substr($raw, 0, $iv_length);
        $encrypted = substr($raw, $iv_length);

        // Decrypt data
        $decrypted = openssl_decrypt($encrypted, 'AES-256-CBC', base64_decode($this->config['crypt_secret']), OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);

        // Unpad data
        $pad = ord(substr($decrypted, -1));
        if ($pad === 0) {
            $decrypted = rtrim($decrypted, chr(0));
        } else {
            $decrypted = substr($decrypted, 0, -$pad);
        }

        return $decrypted;
    }

    /**
     * Inits the configuration
     *
     * @param array $config
     * @throws LogicException
     */
    protected function initConfig(array $config)
    {
        $this->config = array_merge(
            $this->config, array_intersect_key($config, $this->config)
        );

        $this->config['endpoint'] = rtrim($this->config['endpoint'], '/');

        if (empty($this->config['endpoint']) || empty($this->config['api_key']) || empty($this->config['sign_secret'])) {
            throw new LogicException('Some config parameters are missing.');
        }
    }

    /**
     * Preprocess resource and params, reemplacing variables
     *
     * @param string $resource
     * @param array $params
     * @return array
     */
    protected function processResourceAndParams($resource, array $params)
    {
        $resource = trim($resource, '/');

        foreach ($params as $key => $value) {
            if (!is_scalar($value)) {
                continue;
            }

            $resource = str_replace('{'. $key .'}', urlencode($value), $resource, $replaced);

            if ($replaced) {
                unset($params[$key]);
            }
        }

        return [$resource, $params];
    }

    /**
     * Performs a cURL request
     *
     * @param string $resource
     * @param array $params
     * @param string $http_method
     * @return Response
     * @throws ApiException
     */
    private function doRequest($http_method, $resource, array $params)
    {
        list($resource, $params) = $this->processResourceAndParams($resource, $params);
        $ts = time();
        $url = $this->config['endpoint'] . '/v2/' . $resource;

        $curl = curl_init();
        $header = fopen('php://temp', 'w+');
        $body = fopen('php://temp', 'w+');

        /* Options */
        curl_setopt($curl, CURLOPT_USERAGENT, 'VAMS API CLIENT/'. file_get_contents(__DIR__ . '/../.version'));
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->config['verify_ssl']); // TRUE for production
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->config['verify_ssl'] ? 2 : 0); // 2 for production
        if ($this->config['ssl_certs_bundle'] !== null) {
            curl_setopt($curl, CURLOPT_CAINFO, $this->config['ssl_certs_bundle']);
        }
        curl_setopt($curl, CURLOPT_WRITEHEADER, $header);
        curl_setopt($curl, CURLOPT_FILE, $body);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'x-auth-user:' . $this->config['api_key'],
            'x-auth-ts:' . $ts,
            'x-auth-hash:' . $this->calculateHash($resource, $params, $ts),
        ]);

        /* Params */
        switch (strtolower($http_method)) {
            case 'get':
                $url = $url . '?' . http_build_query($params);
                break;

            case 'post':
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->parsePostFields($params));
                break;
        }
        curl_setopt($curl, CURLOPT_URL, $url);

        /* Exec */
        if (!curl_exec($curl)) {
            fclose($header);
            fclose($body);

            $error = curl_error($curl);
            $errno = curl_errno($curl);
            curl_close($curl);

            throw new ApiException($error, $errno);
        }

        /* Create response */
        $response = new Response(
            curl_getinfo($curl, CURLINFO_RESPONSE_CODE),
            $body,
            $this->parseHeaders($header)
        );

        /* Close resources */
        fclose($header);
        curl_close($curl);

        /* Check response */
        if ($response->hasWarnings()) {
            foreach ($response->getWarnings() as $warning) {
                $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
                trigger_error($warning . '. Called in ' . $trace[1]['file'] . ' on line ' . $trace[1]['line'] . '.', E_USER_WARNING);
            }
        }

        return $response;
    }

    /**
     * Parses headers from response header
     *
     * @param resource $header
     * @return array
     */
    private function parseHeaders(/*resource*/ $header)
    {
        rewind($header);

        $headers = [];

        $parts = stream_get_contents($header);
        $parts = trim(strstr($parts, "\r\n"));
        $parts = explode("\r\n", $parts);

        foreach ($parts as $part) {
            list($name, $value) = explode(': ', $part, 2);

            $headers[$name][] = $value;
        }

        return $headers;
    }

    /**
     * Parses parameters for POST requests
     *
     * @param array $params
     * @return string|array
     */
    private function parsePostFields(array $params)
    {
        if ($this->processPostFiles($params)) {
            return $this->flattenPostFields($params);
        } else {
            return http_build_query($params);
        }
    }

    /**
     * Processes parameters searching for uploaded files
     *
     * @param array $params
     * @return boolean
     */
    private function processPostFiles(array &$params)
    {
        $has_files = false;

        foreach ($params as $k => &$v) {
            if (is_array($v)) {
                $has_files = $this->processPostFiles($v);
            } elseif ($v instanceof File) {
                $params[$k] = new CURLFile($v->getPath(), $v->getMimetype(), $v->getFilename());
                $has_files = true;
            }
        }

        return $has_files;
    }

    /**
     * Flattens parameters
     *
     * @param array $params
     * @param string $prepend
     * @return array
     */
    private function flattenPostFields(array $params, $prepend = '')
    {
        $return = [];

        foreach ($params as $k => $v) {
            if (is_array($v)) {
                $return = array_merge($return, $this->flattenPostFields($v, $prepend ? $prepend . '[' . $k . ']' : $k));
            } elseif (!is_null($v)) {
                $return[$prepend ? $prepend . '[' . $k . ']' : $k] = $v;
            }
        }

        return $return;
    }

    /**
     * Calculates the request signature
     *
     * @param string $resource
     * @param array $params
     * @param int $ts
     * @return string
     */
    protected function calculateHash($resource, array $params, $ts)
    {
        // Add auth params
        $params['api_ts'] = $ts;
        $params['api_secret'] = $this->config['sign_secret'];

        // Order params alphabetically
        ksort($params);

        // Build base string
        $base = 'v2/' . $resource . '?' . http_build_query($params);

        // Hash with SHA-256
        return hash('sha256', $base);
    }

}
