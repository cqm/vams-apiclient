<?php

namespace VAMS\ApiClient;

class File
{

    private $path;
    private $mimetype;
    private $filename;

    function __construct($path, $mimetype, $filename)
    {
        $this->path = $path;
        $this->mimetype = $mimetype;
        $this->filename = $filename;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getMimetype()
    {
        return $this->mimetype;
    }

    public function getFilename()
    {
        return $this->filename;
    }

}
