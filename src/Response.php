<?php

namespace VAMS\ApiClient;

use VAMS\ApiClient\exceptions\BadResponseFormatException;

class Response
{

    private $status_code;
    private $stream;
    private $headers = [];
    private $data = null;
    private $warnings = null;

    /**
     * @param int $status_code
     * @param resource $stream
     * @param array $headers
     */
    public function __construct($status_code, $stream, array $headers = [])
    {
        $this->stream = $stream;
        $this->status_code = $status_code;
        $this->setHeaders($headers);
    }

    public function __destruct()
    {
        if (is_resource($this->stream)) {
            fclose($this->stream);
        }
    }

    /**
     * Is the response OK?
     *
     * @return bool
     */
    public function isOk()
    {
        return $this->status_code >= 200 && $this->status_code < 300;
    }

    /**
     * Is the response a Redirect?
     *
     * @return bool
     */
    public function isRedirect()
    {
        return $this->status_code >= 300 && $this->status_code < 400;
    }

    /**
     * Is the response OK?
     *
     * @return bool
     */
    public function isError()
    {
        return $this->status_code >= 400;
    }

    /**
     * Is the response not found?
     *
     * @return bool
     */
    public function isNotFound()
    {
        return $this->status_code === 404;
    }

    /**
     * Response generated when an entity could not be processed due to
     * invalid input data (validation errors).
     *
     * @return bool
     */
    public function isUnprocessable()
    {
        return $this->status_code === 422;
    }


    /**
     * Returns response headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Is the header present?
     *
     * @param string $header
     * @return bool
     */
    public function hasHeader($header)
    {
        return isset($this->headers[$this->normalizeHeaderName($header)]);
    }

    /**
     * Returns the first header or $default if not present.
     *
     * @param string $header
     * @param mixed $default
     * @return mixed
     */
    public function getHeader($header, $default = null)
    {
        $header = $this->normalizeHeaderName($header);

        return isset($this->headers[$header]) ? $this->headers[$header][0] : $default;
    }

    /**
     * Returns all the headers or $default if not present.
     *
     * @param string $header
     * @return array
     */
    public function getAllHeaders($header)
    {
        $header = $this->normalizeHeaderName($header);

        return isset($this->headers[$header]) ? $this->headers[$header] : [];
    }

    /**
     * returns response HTTP status code
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * Response body as stream
     *
     * @return resource
     */
    public function getStream()
    {
        rewind($this->stream);

        return $this->stream;
    }

    public function hasWarnings()
    {
        return $this->hasHeader('X-Vams-Warning');
    }

    /**
     * Return response warnings
     *
     * @return ResponseWarning[]
     */
    public function getWarnings()
    {
        if ($this->warnings === null) {
            $this->warnings = $this->fetchWarnings();
        }

        return $this->warnings;
    }

    /**
     * Response body as string
     *
     * @return string
     */
    public function getBody()
    {
        return stream_get_contents($this->getStream());
    }

    /**
     * Parsed body response
     *
     * @return ?object
     * @throws exceptions\NotParseableResponseException
     */
    public function getData()
    {
        if ($this->data === null && !$this->parseBody()) {
            throw new exceptions\NotParseableResponseException($this, "Response body of type '". $this->getContentType() ."' can not be parsed");
        }

        return $this->data;
    }

    /**
     * Returns VAMS response code (aka error_code)
     *
     * @return int
     * @throws exceptions\NotParseableResponseException
     */
    public function getErrorCode()
    {
        return $this->getData()->error_code;
    }

    /**
     * Returns VAMS response message (aka error_message)
     *
     * @return string
     * @throws exceptions\NotParseableResponseException
     */
    public function getErrorMessage()
    {
        return $this->getData()->error_message;
    }

    /**
     * Returns parsed data from the response (aka result)
     *
     * @return mixed
     * @throws exceptions\NotParseableResponseException
     */
    public function getResult()
    {
        return $this->getData()->result;
    }

    /**
     * Mime type of the response
     *
     * @return ?string
     */
    public function getContentType()
    {
        $content_type = $this->getHeader('Content-Type');

        if ($content_type === null || !strpos($content_type, ';')) {
            return $content_type;
        }

        return strstr($content_type, ';', true);
    }

    /**
     * @throws exceptions\NotParseableResponseException
     */
    public function __isset($name)
    {
        return isset($this->getData()->$name);
    }

    /**
     * @throws exceptions\NotParseableResponseException
     */
    public function __get($name)
    {
        if (isset($this->getData()->$name)) {
            return $this->getData()->$name;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property: ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    /**
     * @param string $header
     * @return string
     */
    protected function normalizeHeaderName($header)
    {
        return strtr($header, '_ABCDEFGHIJKLMNOPQRSTUVWXYZ', '-abcdefghijklmnopqrstuvwxyz');
    }

    private function setHeaders(array $headers)
    {
        $this->headers = [];

        foreach ($headers as $name => $value) {
            $this->headers[$this->normalizeHeaderName($name)] = $value;
        }
    }

    /**
     * @throws BadResponseFormatException
     */
    private function parseBody()
    {
        if ($this->getContentType() === 'application/json') {
            return $this->parseJsonBody();
        }

        return false;
    }

    /**
     * @throws BadResponseFormatException
     */
    private function parseJsonBody()
    {
        $this->data = @json_decode($this->getBody());

        if (json_last_error()) {
            throw new BadResponseFormatException(
                $this,
                function_exists('json_last_error_msg') ? json_last_error_msg() : 'Error parsing response',
                json_last_error()
            );
        }

        return true;
    }

    private function fetchWarnings()
    {
        if (!$this->hasWarnings()) {
            return [];
        }

        return array_map([ResponseWarning::class, 'fromHeader'], $this->getAllHeaders('X-Vams-Warning'));
    }

}
