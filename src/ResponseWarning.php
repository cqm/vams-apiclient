<?php

namespace VAMS\ApiClient;

use UnexpectedValueException;

class ResponseWarning
{

    /** @var string */
    private $type;
    /** @var string */
    private $message;

    /**
     * @param ?string $type
     * @param string $message
     */
    public function __construct($type, $message)
    {
        $this->type = $type;
        $this->message = $message;
    }

    /**
     * @return ?string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function __toString()
    {
        $string = '';

        if (!empty($this->type)) {
            $string = ucfirst($this->type) . ': ';
        }

        $string .= $this->message;

        return $string;
    }

    /**
     * @param string $header
     * @return self
     * @throws UnexpectedValueException when passed header is malformed
     */
    static public function fromHeader($header)
    {
        if (strpos($header, ' ') === false) {
            throw new UnexpectedValueException('Malformed warning header: '. $header);
        }

        list($type, $message) = explode(' ', $header, 2);

        if (substr_compare($message, '"', 0, 1) !== 0 || substr_compare($message, '"', -1, 1) !== 0) {
            throw new UnexpectedValueException('Malformed warning header: '. $header);
        }

        if ($type === '-') {
            $type = null;
        }

        return new self($type, substr($message, 1, -1));
    }

}