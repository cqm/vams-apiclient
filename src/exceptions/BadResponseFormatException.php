<?php

namespace VAMS\ApiClient\exceptions;

class BadResponseFormatException extends NotParseableResponseException
{

    private $response;

    public function __construct($response, $message = '', $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getResponseText()
    {
        return $this->response->getBody();
    }

}
